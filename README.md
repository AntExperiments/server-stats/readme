# README

This is a project, which allows the visitors of my website, to see the usage of my servers. 

1. [Server-side script](https://gitlab.com/AntExperiments/server-stats/server-side-script)
    - WIP : Replace with actual application (maybe Python?)
    - This raw data can already be used in e.g. Home Assistant
2. [MQTT to WS bridge](https://gitlab.com/AntExperiments/server-stats/mqtt-to-websocket)
    - Basically works by having two independent processes running parallel
        - A "chat" framework, which relays received messages to every connected user
        - The MQTT logic, which sends a message to the chat framework to update
3. [Website to receive and display data](https://gitlab.com/ConfusedAnt/website)
    - Keeping a connection open, and then updating the text once a message has been received

Checkout the current ToDo-List here: [craft.do/s/JOSz7h5glH7kZH](https://www.craft.do/s/JOSz7h5glH7kZH)
